## coral-user 12 S2B1.211112.006 7934767 release-keys
- Manufacturer: google
- Platform: msmnile
- Codename: coral
- Brand: google
- Flavor: coral-user
- Release Version: 12
- Id: S2B1.211112.006
- Incremental: 7934767
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/S2B1.211112.006/7934767:user/release-keys
- OTA version: 
- Branch: coral-user-12-S2B1.211112.006-7934767-release-keys
- Repo: google_coral_dump_19544


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
